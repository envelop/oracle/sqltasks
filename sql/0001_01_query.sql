select address, count(*) as wnft_count from (
        --First subquery is for wNFT version 1
    SELECT v_wnft.first_owner AS address
        FROM public.v_wnft  
           JOIN public.raw_envelopv1 env ON env.txhash  = v_wnft.create_tx
                 and env.chainid  = v_wnft.chain_id  
        WHERE  v_wnft.wnft_type != -1
                AND v_wnft.first_owner != 'first_owner_inserted_manually'
                AND env.chainid  = 137
                AND env.zerotopic = '0xa90a3b8dae41ae10a708d32fec7bf12da5c90879c98b9c4cca3c8fba91ddf493'
            AND  env.blocknumber BETWEEN 
        ---------1st period --------
--                          public.get_block_for_date(56, '2021-10-21', false)  --from
--                  AND public.get_block_for_date(56, '2022-03-21', false)  --to
        -- ---------2nd period --------
--                  public.get_block_for_date(1, '2022-03-22', false)  --from
--              AND public.get_block_for_date(1, '2022-09-22', false)  --to
        ---------3rd period --------
               public.get_block_for_date(137, '2022-09-22', false)  --from
           AND public.get_block_for_date(137, '2023-03-23', false)  --to

        union all

        --Second subquery is for wNFT version 0 
        SELECT v_wnft.first_owner AS address
        FROM public.v_wnft 
                  -- Check that wNFT was minted (because some fake events are exist)
                  inner join raw_transfer721 t721 on t721.txhash = v_wnft.create_tx
                          and v_wnft.contract_address = lower(t721.logger) 
                          and v_wnft.token_id = hex_to_int(right(t721.topic3, 64))
                          and v_wnft.chain_id = t721.chainid 
                          and t721.topic1 ='0x0000000000000000000000000000000000000000000000000000000000000000'
        WHERE  v_wnft.chain_id  = 137 and v_wnft.wnft_type = -1
                AND v_wnft.first_owner != 'first_owner_inserted_manually'
                AND  t721.blocknumber BETWEEN 
        ---------1st period --------
--                          public.get_block_for_date(56, '2021-10-21', false)  --from
--                  AND public.get_block_for_date(56, '2022-03-21', false)  --to
        -- ---------2nd period --------
--                  public.get_block_for_date(1, '2022-03-22', false)  --from
--              AND public.get_block_for_date(1, '2022-09-22', false)  --to
        ---------3rd period --------
               public.get_block_for_date(137, '2022-09-22', false)  --from
           AND public.get_block_for_date(137, '2023-03-23', false)  --to
) R
GROUP BY 1
order by 2 desc
