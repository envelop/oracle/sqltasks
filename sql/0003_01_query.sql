select * from (
    SELECT 1 as chain_id, p1.holder, GREATEST(p1.balance_in::integer, p1.balance_out::integer) as maxB 
	from public.get_erc20_saldo_table_by_datetime(
			1, 
			'0x7728cd70b3dd86210e2bd321437f448231b81733', 
			'2021-10-21', 
			'2022-03-21', 
			18
	) p1
	union all
	SELECT 56 as chain_id, p1.holder, GREATEST(p1.balance_in::integer, p1.balance_out::integer) as maxB 
	from public.get_erc20_saldo_table_by_datetime(
			56, 
			'0x7728cd70b3dd86210e2bd321437f448231b81733', 
			'2021-10-21', 
			'2022-03-21',
		    18
	) p1
	union all
	SELECT 137 as chain_id, p1.holder, GREATEST(p1.balance_in::integer, p1.balance_out::integer) as maxB
	from public.get_erc20_saldo_table_by_datetime(
			137, 
			'0x432cdbC749FD96AA35e1dC27765b23fDCc8F5cf1', 
			'2021-10-21', 
			'2022-03-21', 
			18
	) p1
) R
where R.maxB >0
ORDER BY 1 asc,3 desc



---------1st period --------
--                         public.get_block_for_date(56, '2021-10-21', false)  --from
--                 AND public.get_block_for_date(56, '2022-03-21', false)  --to
        -- ---------2nd period --------
        --         public.get_block_for_date(56, '2022-03-22', false)  --from
        --     AND public.get_block_for_date(56, '2022-09-22', false)  --to
        ---------3rd period --------
        --        public.get_block_for_date(56, '2022-09-22', false)  --from
        --    AND public.get_block_for_date(56, '2023-03-23', false)  --to