# SQL Tasks
This repo exist for organize work with data reports. 

## How to process issues(nnnn)
1. Read task in issue. Ask in comments if you don't clearly understand task.
Please move task to **InWork** [board](https://gitlab.com/envelop/oracle/sqltasks/-/boards)
2. Implement and check your query with appropriate data source. Use as more 
comments as possible. Track you time.
3. Safe result in sql folder with proper naming: `nnnn_xx_query.sql`, where
- nnnn - task number, 
- xx - data source code(see bellow).
4. In commit message you need note issue number (e.g. #12 )
5. You can make some important comments in task.
4. Please change **Assignee** field in issue form to assigner only after you
 check that sql working correct(by your meaning).
5. Issue initiator must move task to **Closed** [board](https://gitlab.com/envelop/oracle/sqltasks/-/boards)
If the initiator is not satisfied with the result he(she) can reassign issue back.


## Data Source Codes (xx)
### 01 - Envelop Oracle
- Credentials - ask admin
### 02 - Dune Platform
- Docs: https://dune.com/docs/
- Credentials - ask admin
### 03 - Flipsidecrypto
- Docs: https://docs.flipsidecrypto.com/
